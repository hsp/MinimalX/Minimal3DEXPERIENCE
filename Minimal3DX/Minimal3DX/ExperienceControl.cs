﻿using System;

namespace Minimal3DX
{
    class ExperienceControl
    {
        ExperienceControl()
        {
                Console.WriteLine("Minimal3DX");
                Console.WriteLine("CATIA/3DEXPERIENCE API Beispiel\n\n");

            try
            {
                ExperienceConnection myExperienceConnection = new ExperienceConnection();

                Console.ForegroundColor = ConsoleColor.Green;

                myExperienceConnection.ConnectTo3DExperience();
                Console.WriteLine("Laufenden 3DEXPERIENCE-Prozess gefunden! \n");

                //3Dx Einstellungen anpassen
                myExperienceConnection.einstellungenAnpassen();

                Console.WriteLine("\n=================================");
                Console.WriteLine("3Dx Part:");

                // Öffne eine neue 3D-Form/3DShape
                myExperienceConnection.Erzeuge3DShape();
                Console.WriteLine("1 3DShape erzeugt! ");

                // Erstelle eine Skizze
                myExperienceConnection.ErstelleLeereSkizze("Profil");
                Console.WriteLine("2 Skizze erzeugt");

                // Generiere ein Profil
                myExperienceConnection.ErzeugeProfil(20, 10);
                Console.WriteLine("3 Profil erzeugt");

                // Extrudiere Balken
                myExperienceConnection.ErzeugeBalken(100);
                Console.WriteLine("4 Balken erzeugt");
                Console.WriteLine("=================================");

                Console.ResetColor();
            }
            catch (Exception ex)
            {
                // Anzeigen der Fehlermeldung.
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.ResetColor();
                Console.WriteLine("\nDas Programm ist beendet.");
                Console.WriteLine("Beliebige Taste zum Beenden drücken.");
                Console.ReadLine();
            }
        }


        static void Main(string[] args)
        {
            new ExperienceControl();
        }
    }
}
