﻿using CATPLMEnvBuild;
using HybridShapeTypeLib;
using INFITF;
using MECMOD;
using PARTITF;
using ProductStructureClientIDL;
using System;


namespace Minimal3DX
{
    public class ExperienceConnection
    {
        private INFITF.Application hsp_experienceApp;
        private Editor hsp_experienceEditor;
        private Part hsp_Part;
        private Sketches hsp_Sketches;
        private Sketch hsp_catiaSkizze;

        private ShapeFactory SF;
        private HybridShapeFactory HSF;

        /// <summary>
        /// Die Methode versucht, eine laufende 3DEXPERIENCE-Anwendung zu finden. Falls keine gefunden werden sollte, tritt eine "Exception" auf, welche abgefangen werden sollte.
        /// </summary>
        public void ConnectTo3DExperience()
        {
            try
            {
                object experienceObject = Marshal2.GetActiveObject(
                    "CATIA.Application");
                hsp_experienceApp = (Application) experienceObject;

                // Prüfen, ob auch wirklich 3DEXPERIENCE gefunden wurde.
                if (hsp_experienceApp.get_Name() != "3DEXPERIENCE")
                {
                    throw new Exception();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("3DEXPERIENCE konnte nicht gefunden werden. Schließen Sie bitte ggf. weitere CATIA-Prozesse. " + ex.Message);
            }
        }

        /// <summary>
        /// Die Methode versucht, eine 3DShape zu erzeugen.
        /// </summary>
        public void Erzeuge3DShape()
        {
            try
            {
                PLMNewService newService = (PLMNewService)hsp_experienceApp.GetSessionService("PLMNewService");

                // Ein "Editor"-Objekt wird als Outpout zurückgeliefert.
                newService.PLMCreate("3DShape", out hsp_experienceEditor);

                // Part-Objekt abfragen.
                hsp_Part = (Part) hsp_experienceEditor.ActiveObject;

                // 3DShape bekommen.
                VPMRepReference myProductRepresentationReference = (VPMRepReference) hsp_Part.Parent;

                // ID
                myProductRepresentationReference.SetAttributeValue("PLM_ExternalID", "Profil_HSP_ID");
                myProductRepresentationReference.SetAttributeValue("Name", "Profil_HSP");

                hsp_Part.Update();
            }
            catch (Exception ex)
            {
                throw new Exception("3DEXPERIENCE Shape konnte nicht erzeugt werden - Fehler: " + ex.Message);
            }

        }


        /// <summary>
        /// Die Methode versucht, eine leere Skizze zu erzeugen.
        /// </summary>
        public void ErstelleLeereSkizze(String profileName, double offset = 90.0)
        {
            try
            {
			
            // Factories für das Erzeugen von Modellelementen (Std und Hybrid)
            SF = (ShapeFactory)hsp_Part.ShapeFactory;
            HSF = (HybridShapeFactory)hsp_Part.HybridShapeFactory;

            // geometrisches Set auswaehlen und umbenennen
            HybridBodies catHybridBodies1 = hsp_Part.HybridBodies;
            HybridBody catHybridBody1;
            try
            {
                catHybridBody1 = catHybridBodies1.Item("Geometrisches Set.1");
            }
            catch (Exception)
            {
                try
                {
                    // Neues Geometrisches Set hinzufügen.
                    catHybridBody1 = catHybridBodies1.Add();

                    // Name des neuene geometrischen Sets ändern.
                    catHybridBody1.set_Name("Geometrisches Set.1");

                    // Part aktualisieren.
                    hsp_Part.Update();
                }
                catch
                {
                    throw new Exception("Kein geometrisches Set gefunden! " + Environment.NewLine +
                        "Ein PART manuell erzeugen und ein darauf achten, dass 'Geometisches Set' aktiviert ist.");
                }
            }
            catHybridBody1.set_Name(profileName);

            // neue Skizze im ausgewaehlten geometrischen Set auf eine Offset Ebene legen
            hsp_Sketches = catHybridBody1.HybridSketches;
            OriginElements catOriginElements = hsp_Part.OriginElements;
            HybridShapePlaneOffset hybridShapePlaneOffset1 = HSF.AddNewPlaneOffset(
                (Reference)catOriginElements.PlaneYZ, offset, false);
            hybridShapePlaneOffset1.set_Name("OffsetEbene");
            catHybridBody1.AppendHybridShape(hybridShapePlaneOffset1);
            hsp_Part.InWorkObject = hybridShapePlaneOffset1;
            hsp_Part.Update();

            HybridShapes hybridShapes1 = catHybridBody1.HybridShapes;
            Reference catReference1 = (Reference)hybridShapes1.Item("OffsetEbene");

            hsp_catiaSkizze = hsp_Sketches.Add(catReference1);

            // Achsensystem in Skizze erstellen 
            ErzeugeAchsensystem();

            // Part aktualisieren
            hsp_Part.Update();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }			 
        }

        /// <summary>
        /// Die Methode erzeugt ein Achsensystem.
        /// </summary>
        private void ErzeugeAchsensystem()
        {
            object[] arr = new object[] {0.0, 0.0, 0.0,
                                         0.0, 1.0, 0.0,
                                         0.0, 0.0, 1.0 };
            hsp_catiaSkizze.SetAbsoluteAxisData(arr);
        }

        /// <summary>
        /// Die Methode erzeugt ein Profil.
        /// </summary>
        public void ErzeugeProfil(double breite, double hoehe)
        {
            // Skizze umbenennen
            hsp_catiaSkizze.set_Name("Rechteck");

            // Rechteck in Skizze einzeichnen
            // Skizze oeffnen
            Factory2D catFactory2D1 = hsp_catiaSkizze.OpenEdition();

            // Rechteck erzeugen

            // erst die Punkte
            Point2D catPoint2D1 = catFactory2D1.CreatePoint(-breite / 2, hoehe / 2);
            Point2D catPoint2D2 = catFactory2D1.CreatePoint(breite / 2, hoehe / 2);
            Point2D catPoint2D3 = catFactory2D1.CreatePoint(breite / 2, -hoehe / 2);
            Point2D catPoint2D4 = catFactory2D1.CreatePoint(-breite / 2, -hoehe / 2);

            // dann die Linien
            Line2D catLine2D1 = catFactory2D1.CreateLine(-breite / 2, hoehe / 2, breite / 2, hoehe / 2);
            catLine2D1.StartPoint = catPoint2D1;
            catLine2D1.EndPoint = catPoint2D2;

            Line2D catLine2D2 = catFactory2D1.CreateLine(breite / 2, hoehe / 2, breite / 2, -hoehe / 2);
            catLine2D2.StartPoint = catPoint2D2;
            catLine2D2.EndPoint = catPoint2D3;

            Line2D catLine2D3 = catFactory2D1.CreateLine(breite / 2, -hoehe / 2, -breite / 2, -hoehe / 2);
            catLine2D3.StartPoint = catPoint2D3;
            catLine2D3.EndPoint = catPoint2D4;

            Line2D catLine2D4 = catFactory2D1.CreateLine(-breite / 2, -hoehe / 2, -breite / 2, hoehe / 2);
            catLine2D4.StartPoint = catPoint2D4;
            catLine2D4.EndPoint = catPoint2D1;

            // Skizzierer verlassen
            hsp_catiaSkizze.CloseEdition();
            // Part aktualisieren
            hsp_Part.Update();
        }

        /// <summary>
        /// Die Methode erzeugt aus dem Profil durch eine Extrusion einen Balken.
        /// </summary>
        public void ErzeugeBalken(double laenge)
        {
            try
            {
                // Hauptkoerper in Bearbeitung definieren
                hsp_Part.InWorkObject = hsp_Part.MainBody;

                // Block(Balken) erzeugen
                Pad catPad1 = SF.AddNewPad(hsp_catiaSkizze, laenge);

                // Block umbenennen
                catPad1.set_Name("Balken");

                // Part aktualisieren
                hsp_Part.Update();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Methode zum einstellen und Speichern der Einstellungen innerhalb von 3Dx
        /// 1. Automatisch ein Achsensystem erzeugen bei einem neuen Part
        /// 2. Automatisch ein Geometrisches Set erzeugen bei einem neuen Part
        /// 3. Hybridkonstruktionen ermöglichen
        /// </summary>
        public void einstellungenAnpassen()
        {
            try
            {
                // Konsolen-Farbe ändern
                ConsoleColor originalColor = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.White;

                Console.WriteLine("=================================");
                Console.WriteLine("Einstellungen:");
                SettingControllers mySettings = hsp_experienceApp.SettingControllers;

                // Das passende SettingRepository erhalten. Entspricht dem Menüabschnitt "Einstellungen\Infrastruktur\3D Shape Infrastructure"
                SettingRepository shapeSettingRepository = (SettingRepository)mySettings.Item("3DShapeInfrastructure3DShape");

                // Achsensystem
                bool NewWithAxisSystemEnabled = (bool)shapeSettingRepository.GetAttr("NewWithAxisSystem");
                if (NewWithAxisSystemEnabled == false)
                {
                    shapeSettingRepository.PutAttr("NewWithAxisSystem", true);
                    Console.WriteLine("Einstellung für das Achsensystem wurde auf \"True\" geändert.");
                }
                else
                {
                    Console.WriteLine("Die Einstellungen für das Achsensystem sind aktuell");
                }

                // Geometisches Set
                bool NewWithGSEnabled = (bool)shapeSettingRepository.GetAttr("NewWithGS");
                if (NewWithGSEnabled == false)
                {
                    shapeSettingRepository.PutAttr("NewWithGS", true);
                    Console.WriteLine("Einstellung für das Geometrische Set wurde auf \"True\" geändert.");
                }
                else
                {
                    Console.WriteLine("Die Einstellungen für das Geometrische Set sind aktuell.");
                }

                // Hybridkonstruktionen
                bool HybridDesignModeEnabled = (bool)shapeSettingRepository.GetAttr("HybridDesignMode");
                if (HybridDesignModeEnabled == false)
                {
                    shapeSettingRepository.PutAttr("HybridDesignMode", true);
                    Console.WriteLine("Einstellung für Hybridkonstruktionen wurde auf \"True\" geändert.");
                }
                else
                {
                    Console.WriteLine("Die Einstellungen für Hybridkonstruktionen sind aktuell.");
                }

                shapeSettingRepository.SaveRepository();

                Console.WriteLine("=================================");

                // Konsolen-Farbe zurücksetzen auf ursprünglichen Wert
                Console.ForegroundColor = originalColor;

            }
            catch (Exception)
            {
                throw new Exception("Beim Bearbeiten der Einstellungen ist ein Fehler aufgetreten.");
            }
        }

    }
}