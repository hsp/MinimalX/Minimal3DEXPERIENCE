# Minimal3DX

Automatisierung von Catia in der 3DEXPERIENCE Platform via COM API. 

## Funktion

Durch die Konsolenanwendung wird eine Verbindung zu Catia aufgebaut und nacheinander 
- ein Part erzeugt
- im Part eine Skizze erzeugt
- in der Skizze ein Profil erzeugt
- mit dem Profil ein Balken extrudiert (Pad)

## Getting started
Der Code wird in einem Video in Moodle erklärt. Für eine eigene Anwendung müssen die Bibilotheken im Ordner "References" eingebunden werden. 
